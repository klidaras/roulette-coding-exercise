package com.gamesys.roulette;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Bet {

	private final BetType betType;
	private final int ruletteSlotSelection;
	private Player player;
	private final LocalDateTime timestapm;
	private BigDecimal betAmount = new BigDecimal("0.0");
	private BigDecimal betPayoffAmount = new BigDecimal("0.0");

	public Bet(BetType betType, int ruletteSelection, BigDecimal betAmount,
			Player player) {
		this.betType = betType;
		this.ruletteSlotSelection = ruletteSelection;
		this.betAmount = betAmount;
		this.player = player;
		this.timestapm = LocalDateTime.now();
	}

	public String getSelectedBetTypeDescription() {
		String desc = "";
		if (betType == BetType.EVEN)
			desc = "EVEN";
		else if (betType == BetType.ODD)
			desc = "ODD";
		else if (betType == BetType.NUMBER) {
			desc = String.valueOf(ruletteSlotSelection);
		}
		return desc;
	}

	public String getSelectedOutcomeDescription() {
		return (betPayoffAmount.compareTo(BigDecimal.ZERO) == 0) ? "LOSE"
				: "WIN";
	}

	public BetType getBetType() {
		return betType;
	}

	public int getRuletteSlotSelection() {
		return ruletteSlotSelection;
	}

	public Player getPlayer() {
		return player;
	}

	public LocalDateTime getTimestapm() {
		return timestapm;
	}

	public BigDecimal getBetAmount() {
		return betAmount;
	}

	public BigDecimal getBetPayoffAmount() {
		return betPayoffAmount;
	}

	public void setBetPayoffAmount(BigDecimal betPayoffAmount) {
		this.betPayoffAmount = betPayoffAmount;
	}

	@Override
	public String toString() {
		return "Bet [betType=" + betType + ", ruletteSlotSelection="
				+ ruletteSlotSelection + ", player=" + player.getName()
				+ ", betAmount=" + betAmount + ", betPayoffAmount="
				+ betPayoffAmount + "]";
	}

}
