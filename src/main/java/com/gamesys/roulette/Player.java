package com.gamesys.roulette;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Player {

	private String name;

	private BigDecimal totalWinnings = new BigDecimal("0.0");
	private BigDecimal totalBets = new BigDecimal("0.0");
	

	public Player(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getTotalWinnings() {
		return totalWinnings;
	}

	public void setTotalWinnings(BigDecimal totalWinnings) {
		this.totalWinnings = totalWinnings;
	}

	public BigDecimal getTotalBets() {
		return totalBets;
	}

	public void setTotalBets(BigDecimal totalBets) {
		this.totalBets = totalBets;
	}

	public void updateTotalAndKeepHistoryOfBets(Bet bet) {

		try {
			totalWinnings = totalWinnings.add(bet.getBetPayoffAmount());
			totalBets = totalBets.add(bet.getBetAmount());			
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}



	@Override
	public String toString() {
		return "Player [name=" + name + ", totalWinnings=" + totalWinnings
				+ ", totalBets=" + totalBets + "]";
	}

}
