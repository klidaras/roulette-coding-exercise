package com.gamesys.roulette;

import java.util.Random;

/**
 * The random position generator has been coded separately in order to help with
 * the tests
 * 
 * @author vassilios
 *
 */
public class RoulettePositionGenerator {

	private final Random randomGenerator = new Random(
			System.currentTimeMillis());

	int generatePostion() {
		return randomGenerator.nextInt(36) + 1;
	}

	public RoulettePositionGenerator() {
	}

}
