package com.gamesys.roulette;

import java.math.BigDecimal;

public class PayOffCalculator {

	private final BigDecimal TIMES_THIRTY_SIX = new BigDecimal(36);
	private final BigDecimal TIMES_TWO = new BigDecimal(2);

	BigDecimal CalculatePayOff(int selectedRoulettePosition, Bet bet) {

		BigDecimal amountCalculated = new BigDecimal("0.0");

		if (bet.getBetType() == BetType.NUMBER) {
			if (selectedRoulettePosition == bet.getRuletteSlotSelection()) {
				amountCalculated = bet.getBetAmount()
						.multiply(TIMES_THIRTY_SIX);
			}
		} else if (bet.getBetType() == BetType.EVEN
				&& (selectedRoulettePosition % 2) == 0) {
			amountCalculated = bet.getBetAmount().multiply(TIMES_TWO);
		} else if (bet.getBetType() == BetType.ODD
				&& (selectedRoulettePosition % 2) != 0) {
			amountCalculated = bet.getBetAmount().multiply(TIMES_TWO);
		}

		return amountCalculated;
	}

}
