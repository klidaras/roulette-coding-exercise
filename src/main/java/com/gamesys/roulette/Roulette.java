package com.gamesys.roulette;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * The roulette class will model the game.
 * 
 * @author vassilios
 *
 */

public class Roulette {

	/**
	 * Is good to define the maximum number of bets the roulette can take so we
	 * don't get into issues of memory, latency etc. If the number of bets
	 * exceeds that size it will just block instead of having service delays etc
	 */

	static final Logger logger = LogManager.getLogger(Roulette.class.getName());

	public static final int MAX_SIZE = 100000;
	public static final int ROULETTE_TIME_INTERVAL_30_SECONDS = 30;
	public static final int ROULETTE_TIME_INTERVAL_0_SECOND = 1;
	static final String PLAYERS_FILENAME = "/players.txt";

	public static final BigDecimal ZERO_VALUE = new BigDecimal("0");
	public static final BigDecimal MAX_ALLOWED_BET = new BigDecimal("1000");

	public static final int ROULETTE_MIN_POS = 1;
	public static final int ROULETTE_MAX_POS = 36;

	private ConcurrentMap<String, Player> playerMap = new ConcurrentHashMap<String, Player>();
	private BlockingQueue<Bet> rouletteQueue = new ArrayBlockingQueue<Bet>(
			MAX_SIZE);

	private RoulettePositionGenerator roulettePositionGenerator = new RoulettePositionGenerator();

	private final ScheduledExecutorService scheduledService = Executors
			.newScheduledThreadPool(10);
	private ScheduledFuture<?> scheduledTimer;

	private boolean singleSpinMode = false; // used in test scripts
	private GameStatus gameStatus;
	private int timerDelay = ROULETTE_TIME_INTERVAL_30_SECONDS;
	private TimeUnit timeUnit = TimeUnit.SECONDS;
	private DisplayMode displayMode = DisplayMode.BET_BREAKDOWN_DISPLAY;
	private boolean displayStatsAndDiagnostics = false;

	public void initialize() {
		scheduledTimer = scheduledService.scheduleAtFixedRate(new Runnable() {
			public void run() {
				turnTheWheel();
			}
		}, 0, timerDelay, timeUnit);

		gameStatus = GameStatus.RUNNING;
	}

	public synchronized void stopTheRoulette() {
		scheduledTimer.cancel(false);
		gameStatus = GameStatus.STOPPED;
	}

	public GameStatus getGameStatus() {
		return gameStatus;
	}

	/**
	 * This is the main logic of the roulette The threading aspect is taken care
	 * from ArrayBlockingQueue When the queue is drained it correctly locks any
	 * writes, it should be very fast.
	 */
	void turnTheWheel() {

		List<Bet> listOfBets = new ArrayList<Bet>();
		rouletteQueue.drainTo(listOfBets); // the operation internally while it
											// lasts, locks

		if (displayStatsAndDiagnostics) {
			logger.debug("Inside turnTheWheel() method, we have "
					+ listOfBets.size() + " bets to process");
			displayStats("Before processing the bets", listOfBets);
		}

		int selectedRoulettePosition = roulettePositionGenerator
				.generatePostion();

		PayOffCalculator payOffCalculator = new PayOffCalculator();

		for (Bet bet : listOfBets) {

			BigDecimal calculatedOPayOff = payOffCalculator.CalculatePayOff(
					selectedRoulettePosition, bet);
			bet.setBetPayoffAmount(calculatedOPayOff);

			Player player = bet.getPlayer();
			player.updateTotalAndKeepHistoryOfBets(bet);

			if (displayStatsAndDiagnostics) {
				logger.debug(" calculatedOPayOff is "
						+ calculatedOPayOff.toString() + "  bet = "
						+ bet.toString());
				logger.debug(" updated player tot winning is "
						+ player.getTotalWinnings() + " and tot bets is "
						+ player.getTotalBets());
			}
		}

		if (displayStatsAndDiagnostics) {
			displayStats("After processing the bets", listOfBets);
		}

		printResultsOfTheRound(selectedRoulettePosition, listOfBets);

		// remove the bets processed, jvm will do it anyway, is added here for
		// clarity
		listOfBets = null;

		if (singleSpinMode == true) {
			stopTheRoulette();
		}

	}

	/**
	 * 
	 * @param selectedRoulettePosition
	 * @param listOfBets
	 */
	public void printResultsOfTheRound(int selectedRoulettePosition,
			List<Bet> listOfBets) {

		if (this.displayMode == DisplayMode.BET_BREAKDOWN_DISPLAY) {
			printResultsBrokenByBet(selectedRoulettePosition, listOfBets);
		} else if (this.displayMode == DisplayMode.PLAYER_SUMMARY_DISPLAY) {
			printResultsBrokenByPlayer();
		}

	}

	/**
	 * 
	 * @param selectedRoulettePosition
	 * @param listOfBets
	 */
	public void printResultsBrokenByBet(int selectedRoulettePosition,
			List<Bet> listOfBets) {

		System.out.println("\n");
		System.out.println("Number: " + selectedRoulettePosition);
		System.out.println(String.format("%-15s", "Player")
				+ "\tBet\tOutcome\tWinnings");
		System.out.println("---");

		for (Bet bet : listOfBets) {

			System.out.println(String
					.format("%-15s", bet.getPlayer().getName())
					+ "\t"
					+ bet.getSelectedBetTypeDescription()
					+ "\t"
					+ bet.getSelectedOutcomeDescription()
					+ "\t"
					+ bet.getBetPayoffAmount().toString());
		}

	}

	/**
	 * 
	 * @param selectedRoulettePosition
	 * @param listOfBets
	 */
	public void printResultsBrokenByPlayer() {

		System.out.println("\n");
		System.out.println(String.format("%-15s", "Player")
				+ "\tTotal Win\tTotal Bet");
		System.out.println("---");

		Collection<Player> players = playerMap.values();

		for (Player player : players) {
			System.out.println(String.format("%-15s", player.getName())
					+ "\t"
					+ String.format("%-15s", player.getTotalWinnings()
							.toString()) + "\t"
					+ String.format("%-15s", player.getTotalBets().toString()));

		}

	}

	/**
	 * Load users from file, this method can throw various exceptions
	 * 
	 * @throws Exception
	 */
	public void loadPlayersFromFile(String filename) throws Exception {
		String line;
		String cvsSplitBy = ",";

		InputStream in = this.getClass().getResourceAsStream(filename);
		if (in == null) {
			throw new RuntimeException("Could open player file");
		}
		BufferedReader rdr = new BufferedReader(new InputStreamReader(in,
				"UTF-8"));

		while ((line = rdr.readLine()) != null) {

			// use comma as separator
			String[] elements = line.split(cvsSplitBy);

			if (elements.length == 1 || elements.length == 3) {
				Player player = new Player(elements[0]);

				if (elements.length == 1) {
					// we only have
					player.setTotalWinnings(new BigDecimal("0.0"));
					player.setTotalBets(new BigDecimal("0.0"));
				} else if (elements.length == 3) {
					// we also have total win, total bet values
					player.setTotalWinnings(new BigDecimal(elements[1]));
					player.setTotalBets(new BigDecimal(elements[2]));
				}
				// finally
				addPlayer(player.getName(), player);
			}
		}
	}

	/**
	 * Read input from keyboard
	 * 
	 * @throws Exception
	 */
	void readPlayerInput() throws Exception {

		System.out
				.println("\n\nPlease enter bet details as <Name> <Rulette Number Or EVEN or ODD> Amount> and press ENTER: ");

		try {
			BufferedReader bufferRead = new BufferedReader(
					new InputStreamReader(System.in));

			String line = bufferRead.readLine();

			// ignore input with with enter
			if ("".equals(line))
				return;

			// use space as separator
			String splitBySpace = " ";
			String[] elements = line.split(splitBySpace);
			if (elements.length == 0) { // no point continuing
				throw new IllegalArgumentException(
						"Incorrect number of input parameters");
			}

			if (elements.length == 1 && "QUIT".equals(elements[0])) {
				System.out.println("Stopping the game");
				stopTheRoulette();
			} else if (elements.length == 3) {

				String name = elements[0];
				Player player = getPlayer(name);
				if (player == null) {
					throw new RuntimeException(
							"Player name could be identified");
				}

				BetType betType = BetType.NOTSET;

				int ruletteSlotSelection = 0;
				if ("ODD".equals(elements[1])) {
					betType = BetType.ODD;
				} else if ("EVEN".equals(elements[1])) {
					betType = BetType.EVEN;
				} else if (elements[1].matches("\\d+")) {
					try {
						ruletteSlotSelection = Integer.parseInt(elements[1]);

						if (ruletteSlotSelection < ROULETTE_MIN_POS
								|| ruletteSlotSelection > ROULETTE_MAX_POS) {
							throw new RuntimeException(
									"The betting position must be between 1-36");
						}

						betType = BetType.NUMBER;
					} catch (NumberFormatException e) {
						throw new RuntimeException(
								"Cannot identify the number on which to place the bet");
					}
				} else {
					throw new RuntimeException("Cannot identify betting type");
				}

				// validate also the bet amount
				BigDecimal amount = null;
				try {
					amount = new BigDecimal(elements[2]);

					int res = amount.compareTo(ZERO_VALUE);
					if (res == 0 || res == -1) {
						throw new RuntimeException(
								"Bet value must be greater than zero");
					}
					res = amount.compareTo(MAX_ALLOWED_BET);
					if (res == 1) {
						throw new RuntimeException(
								"Betting value must be less than "
										+ MAX_ALLOWED_BET.toString());
					}
				} catch (NumberFormatException e) {
					throw new RuntimeException(
							"Cannot convert into betting amount");
				}

				if (betType == BetType.NOTSET || amount == null
						|| player == null) { // a final check, you never know
					throw new IllegalArgumentException(
							"Incorrect number of input parameters");
				}

				// Enter the bet now in the system
				addBet(new Bet(betType, ruletteSlotSelection, amount, player));

				// and tell the user, we alway need to provide visual feedback
				System.out.println("Bet accepted for player: "
						+ player.getName());

			} else {
				throw new IllegalArgumentException(
						"Incorrect number of input parameters");
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}

	void addPlayer(String name, Player player) {
		playerMap.put(name, player);
	}

	public ConcurrentMap<String, Player> getPlayerMap() {
		return playerMap;
	}

	public BlockingQueue<Bet> getRouletteQueue() {
		return rouletteQueue;
	}

	public Player getPlayer(String string) {
		return playerMap.get(string);
	}

	public void addBet(Bet bet) {
		rouletteQueue.add(bet);
	}

	public RoulettePositionGenerator getRoulettePositionGenerator() {
		return roulettePositionGenerator;
	}

	public void setRoulettePositionGenerator(
			RoulettePositionGenerator roulettePositionGenerator) {
		this.roulettePositionGenerator = roulettePositionGenerator;
	}

	public int getTimerDelay() {
		return timerDelay;
	}

	public void setTimerDelay(int timerDelay) {
		this.timerDelay = timerDelay;
	}

	public void setTimeUnit(TimeUnit timeUnit) {
		this.timeUnit = timeUnit;
	}

	public boolean isSingleSpinMode() {
		return singleSpinMode;
	}

	public void setSingleSpinMode(boolean singleSpinMode) {
		this.singleSpinMode = singleSpinMode;
	}

	public DisplayMode getDisplayMode() {
		return displayMode;
	}

	public void setDisplayMode(DisplayMode displayMode) {
		this.displayMode = displayMode;
	}
	
	public boolean isDisplayStatsAndDiagnostics() {
		return displayStatsAndDiagnostics;
	}

	public void setDisplayStatsAndDiagnostics(boolean displayStatsAndDiagnostics) {
		this.displayStatsAndDiagnostics = displayStatsAndDiagnostics;
	}

	public void displayStats(String message, List<Bet> listOfBets) {

		logger.debug(message);
		Collection<Player> players = playerMap.values();
		for (Player player : players) {
			logger.debug(player.toString());
		}

		if (listOfBets != null) {
			for (Bet bet : listOfBets) {
				logger.debug(bet.toString());
			}
		}
	}

	private void intializeGame() throws Exception {

		loadPlayersFromFile(PLAYERS_FILENAME);

		initialize(); // start the background thread

		// Play the game, continue reading input, to stop, the user needs to
		// type QUIT in the console
		do {
			readPlayerInput();

		} while (getGameStatus() == GameStatus.RUNNING); // repeat until the
															// user types QUIT

		System.out.println("Stopping the program");

	}

	public static void main(String[] args) {

		Roulette roulette = new Roulette();

		try {
			roulette.intializeGame();
		} catch (Exception e) {
			System.out.println("Serious issue, exiting program "
					+ e.getMessage());
		}

	}

}
