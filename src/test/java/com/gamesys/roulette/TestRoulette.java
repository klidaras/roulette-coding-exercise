package com.gamesys.roulette;

import static org.junit.Assert.assertEquals;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

public class TestRoulette {

	Roulette roulette;

	@Rule
	public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();

	@Before
	public void setupGame() {
		roulette = new Roulette();
	}

	@After
	public void cleanupGame() {
		roulette = null;
	}

	/**
	 * Test case Enter single bets for 2 users
	 * 
	 * Inputs Tiki_Monkey 2 3.0 Barbara EVEN 3.0
	 * 
	 * Expected results Based on a draw of number 2 Tiki_Monkey WON 108.0
	 * Barbara WON 6.0
	 * 
	 */
	@Test
	public void testUserInputsSingleBet() throws Exception {

		roulette.loadPlayersFromFile("/test-players-no-history.txt");

		// set the Bets programmatically
		Player playerTiki = roulette.getPlayer("Tiki_Monkey");
		roulette.addBet(new Bet(BetType.NUMBER, 2, new BigDecimal("3.0"),
				playerTiki));

		Player playerBarbara = roulette.getPlayer("Barbara");
		roulette.addBet(new Bet(BetType.EVEN, 0, new BigDecimal("3.0"),
				playerBarbara));

		// stub the random generator
		RoulettePositionGenerator mockedGenerator = mock(RoulettePositionGenerator.class);
		when(mockedGenerator.generatePostion()).thenReturn(2);
		roulette.setRoulettePositionGenerator(mockedGenerator);

		// single mode and immediate execution
		roulette.setTimerDelay(200);
		roulette.setTimeUnit(TimeUnit.MILLISECONDS);
		roulette.setSingleSpinMode(true);

		// start it, it should run once
		roulette.initialize();

		while (roulette.getGameStatus() == GameStatus.RUNNING) {
			Thread.currentThread().sleep(500);
		}

		Player player = roulette.getPlayer("Tiki_Monkey");
		assertEquals("Player name is wrong", "Tiki_Monkey", player.getName());
		assertEquals("Player Tiki_Monkey bet amount is wrong", 108.0, player
				.getTotalWinnings().doubleValue(), 0.0001);

		player = roulette.getPlayer("Barbara");
		assertEquals("Player name is wrong", "Barbara", player.getName());
		assertEquals("Player bet amount is wrong", 6.0, player
				.getTotalWinnings().doubleValue(), 0.0001);
	}

	/**
	 * 
	 * Test case Enter multiple bet with for 2 users, one round of roulette
	 * Enter two bets for 2 users
	 * 
	 * Input Tiki_Monkey 2 3.0 Tiki_Monkey EVEN 2.0 Barbara EVEN 3.0 Barbara ODD
	 * 3.0
	 * 
	 * Expected results Based on a draw of number 2 Tiki_Monkey WON 3.0 * 36 +
	 * 2.0 * 2 --> 112.0 Barbara WON 3.0 *2 + 0 --> 6.0
	 * 
	 */
	@Test
	public void testUserInputsMultipleBets() throws Exception {

		roulette.loadPlayersFromFile("/test-players-no-history.txt");

		// set the Bets programmatically
		Player playerTiki = roulette.getPlayer("Tiki_Monkey");
		roulette.addBet(new Bet(BetType.NUMBER, 2, new BigDecimal("3.0"),
				playerTiki));
		roulette.addBet(new Bet(BetType.EVEN, 2, new BigDecimal("2.0"),
				playerTiki));

		Player playerBarbara = roulette.getPlayer("Barbara");
		roulette.addBet(new Bet(BetType.EVEN, 0, new BigDecimal("3.0"),
				playerBarbara));
		roulette.addBet(new Bet(BetType.ODD, 0, new BigDecimal("2.0"),
				playerBarbara));

		// stub the random generator
		RoulettePositionGenerator mockedGenerator = mock(RoulettePositionGenerator.class);
		when(mockedGenerator.generatePostion()).thenReturn(2);
		roulette.setRoulettePositionGenerator(mockedGenerator);

		// single mode and immediate execution
		roulette.setTimerDelay(200);
		roulette.setTimeUnit(TimeUnit.MILLISECONDS);
		roulette.setSingleSpinMode(true);

		assertEquals("Number of bets should be 4", 4, roulette
				.getRouletteQueue().size());

		// start it, it should run once
		roulette.initialize();

		while (roulette.getGameStatus() == GameStatus.RUNNING) {
			Thread.currentThread().sleep(500);
		}

		Player player = roulette.getPlayer("Tiki_Monkey");
		assertEquals("Player name is wrong", "Tiki_Monkey", player.getName());
		assertEquals("Player Tiki_Monkey bet amount is wrong", 112.0, player
				.getTotalWinnings().doubleValue(), 0.0001);

		player = roulette.getPlayer("Barbara");
		assertEquals("Player name is wrong", "Barbara", player.getName());
		assertEquals("Player bet amount is wrong", 6.0, player
				.getTotalWinnings().doubleValue(), 0.0001);
	}

	/**
	 * 
	 * Test case Enter 1 user, 2 bets, one round of roulette
	 * 
	 * Input Tiki_Monkey ODD 3.0 Tiki_Monkey EVEN 2.0
	 * 
	 * 
	 * Also use historical data Name Tot win tot bet Tiki_Monkey 2.0 1.0
	 * 
	 * 
	 * Expected results, use the player report Player Total Win Total Bet ---
	 * Tiki_Monkey 6.0 7.0 *
	 **/
	@Test
	public void testUserInputsMultipleBetsUseHistory() throws Exception {

		roulette.loadPlayersFromFile("/test-players-with-history.txt");

		// set the Bets programmatically
		Player playerTiki = roulette.getPlayer("Tiki_Monkey");
		roulette.addBet(new Bet(BetType.ODD, 0, new BigDecimal("4.0"),
				playerTiki));
		roulette.addBet(new Bet(BetType.EVEN, 0, new BigDecimal("2.0"),
				playerTiki));

		// stub the random generator
		RoulettePositionGenerator mockedGenerator = mock(RoulettePositionGenerator.class);
		when(mockedGenerator.generatePostion()).thenReturn(2);
		roulette.setRoulettePositionGenerator(mockedGenerator);

		// single mode and immediate execution
		roulette.setTimerDelay(200);
		roulette.setTimeUnit(TimeUnit.MILLISECONDS);
		roulette.setSingleSpinMode(true);

		roulette.setDisplayMode(DisplayMode.PLAYER_SUMMARY_DISPLAY);

		assertEquals("Number of bets should be 2", 2, roulette
				.getRouletteQueue().size());

		// start it, it should run once
		roulette.initialize();

		while (roulette.getGameStatus() == GameStatus.RUNNING) {
			Thread.currentThread().sleep(500);
		}

		Player player = roulette.getPlayer("Tiki_Monkey");
		assertEquals("Player name is wrong", "Tiki_Monkey", player.getName());
		assertEquals("Player Tiki_Monkey Tot winnings is wrong", 6.0, player
				.getTotalWinnings().doubleValue(), 0.0001);
		assertEquals("Player Tiki_Monkey Tot bets is wrong", 7.0, player
				.getTotalBets().doubleValue(), 0.0001);

	}

	/**
	 * 
	 * Test case Enter 2 user, 2 bets, one round of roulette
	 * 
	 * Input Tiki_Monkey ODD 3.0 Tiki_Monkey EVEN 2.0
	 * 
	 * Barbara ODD 5.0 Barbara EVEN 7.0
	 * 
	 * Also use historical data Name Tot win tot bet Tiki_Monkey 2.0 1.0 Barbara
	 * 2.0 2.0
	 * 
	 * 
	 * Expected results, use the player report Based on a draw of number 2
	 * 
	 * Tiki_Monkey Tot win should be 2.0 * 2 + 2 --> 6.0 Tot bets 2 + 4 + 1 -->
	 * 7
	 * 
	 * Barbara Tot win should be 7.0 * 2 + 2 --> 16.0 Tot bets 5 + 7 + 2 --> 14
	 * 
	 * Output should be Player Total Win Total Bet --- Barbara 16.0 14.0
	 * Tiki_Monkey 6.0 7.0
	 */

	@Test
	public void testTwoUserInputMultipleBetsUseHistory() throws Exception {

		roulette.loadPlayersFromFile("/test-players-with-history.txt");

		// set the Bets programmatically
		Player playerTiki = roulette.getPlayer("Tiki_Monkey");
		roulette.addBet(new Bet(BetType.ODD, 0, new BigDecimal("4.0"),
				playerTiki));
		roulette.addBet(new Bet(BetType.EVEN, 0, new BigDecimal("2.0"),
				playerTiki));

		Player playerBarbara = roulette.getPlayer("Barbara");
		roulette.addBet(new Bet(BetType.ODD, 0, new BigDecimal("5.0"),
				playerBarbara));
		roulette.addBet(new Bet(BetType.EVEN, 0, new BigDecimal("7.0"),
				playerBarbara));

		// stub the random generator
		RoulettePositionGenerator mockedGenerator = mock(RoulettePositionGenerator.class);
		when(mockedGenerator.generatePostion()).thenReturn(2);
		roulette.setRoulettePositionGenerator(mockedGenerator);

		// single mode and immediate execution
		roulette.setTimerDelay(200);
		roulette.setTimeUnit(TimeUnit.MILLISECONDS);
		roulette.setSingleSpinMode(true);

		roulette.setDisplayMode(DisplayMode.PLAYER_SUMMARY_DISPLAY);

		assertEquals("Number of bets should be 4", 4, roulette
				.getRouletteQueue().size());

		// start it, it should run once
		roulette.initialize();

		while (roulette.getGameStatus() == GameStatus.RUNNING) {
			Thread.currentThread().sleep(500);
		}

		Player player = roulette.getPlayer("Tiki_Monkey");
		assertEquals("Player name is wrong", "Tiki_Monkey", player.getName());
		assertEquals("Player Tiki_Monkey Tot winnings is wrong", 6.0, player
				.getTotalWinnings().doubleValue(), 0.0001);
		assertEquals("Player Tiki_Monkey Tot bets is wrong", 7.0, player
				.getTotalBets().doubleValue(), 0.0001);

		player = roulette.getPlayer("Barbara");
		assertEquals("Player name is wrong", "Barbara", player.getName());
		assertEquals("Player Barbara Tot winnings is wrong", 16.0, player
				.getTotalWinnings().doubleValue(), 0.0001);
		assertEquals("Player Barbara Tot bets is wrong", 14.0, player
				.getTotalBets().doubleValue(), 0.0001);

	}
}
