package com.gamesys.roulette;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestGameFileInput {

	Roulette roulette;

	@Before
	public void setupGame() {
		roulette = new Roulette();
	}

	@After
	public void cleanupGame() {
		roulette = null;
	}

	/**
	 * Testing with a normal user file
	 */
	@Test
	public void testTheEngineLoadsPlayersWithCorrectlyFormattedFile()
			throws Exception {

		roulette.loadPlayersFromFile("/test-players-no-history.txt");
		assertEquals("The number of players should be two", 2, roulette
				.getPlayerMap().size());
	}

	/**
	 * This test should raise a NumberFormatException exception as the file
	 * contains characters instead of numbers
	 */
	@Test
	public void testTheEngineLoadsPlayersWithIncorrectlyFormattedFile()
			throws Exception {

		try {
			roulette.loadPlayersFromFile("/test-players-not-numeric.txt");
			fail("It should have raised an exception");
		} catch (NumberFormatException e) {
		}
	}

	/**
	 * This test should raise a RuntimeException exception as the file does not
	 * exist
	 */
	@Test
	public void testMissingFileCorrectlyRaisesException() throws Exception {

		try {
			roulette.loadPlayersFromFile("/blah-blah.txt");
			fail("It should have raised an exception");
		} catch (RuntimeException e) {
			assertEquals(
					"It should have created RuntimeException exception with the message 'cannot open file'",
					"Could open player file", e.getMessage());
		}
	}

}
