package com.gamesys.roulette;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;

public class TestGameKeyboardInput {

	Roulette roulette;

	@Rule
	public final TextFromStandardInputStream systemInMock = emptyStandardInputStream();

	@Before
	public void setupGame() {
		roulette = new Roulette();
	}

	@After
	public void cleanupGame() {
		roulette = null;
	}

	/**
	 * Testing user entering the correct details User name Tiki_Monkey Position
	 * 2 Amount 1
	 */
	@Test
	public void testUserInputScenarioWithRuletteValue() throws Exception {

		roulette.loadPlayersFromFile("/test-players-no-history.txt");
		systemInMock.provideText("Tiki_Monkey 2 1.0\n");
		roulette.readPlayerInput();

		Bet placedBet = roulette.getRouletteQueue().peek();
		assertNotNull("Bet is not placed from Userr", placedBet);
		assertEquals("Player name is wrong", "Tiki_Monkey", placedBet
				.getPlayer().getName());
		assertEquals("Player bet selection is wrong", 2,
				placedBet.getRuletteSlotSelection());
		assertEquals("Player bet amount is wrong", 1.0, placedBet
				.getBetAmount().doubleValue(), 0.0001);
	}

	/**
	 * Testing user entering EVEN User name Tiki_Monkey Position 2 Amount 1
	 */
	@Test
	public void testUserInputScenarioEven() throws Exception {

		roulette.loadPlayersFromFile("/test-players-no-history.txt");
		systemInMock.provideText("Tiki_Monkey EVEN 1.0\n");
		roulette.readPlayerInput();

		Bet placedBet = roulette.getRouletteQueue().peek();
		assertNotNull("Bet is not placed from Userr", placedBet);
		assertEquals("Player name is wrong, should be Tiki_Monkey",
				"Tiki_Monkey", placedBet.getPlayer().getName());
		assertEquals("Player bet amount is wrong, should be 1", 1.0, placedBet
				.getBetAmount().doubleValue(), 0.0001);
		assertTrue("Player bet selection is wrong, should be EVEN",
				placedBet.getBetType() == BetType.EVEN);

	}

	/**
	 * Testing user entering ODD User name Tiki_Monkey Position 2 Amount 1
	 */
	@Test
	public void testUserInputScenarioOdd() throws Exception {

		roulette.loadPlayersFromFile("/test-players-no-history.txt");
		systemInMock.provideText("Tiki_Monkey ODD 1.0\n");
		roulette.readPlayerInput();

		Bet placedBet = roulette.getRouletteQueue().peek();
		assertNotNull("Bet is not placed from Userr", placedBet);
		assertEquals("Player name is wrong", "Tiki_Monkey", placedBet
				.getPlayer().getName());
		assertEquals("Player bet amount is wrong, should be 1.0", 1.0,
				placedBet.getBetAmount().doubleValue(), 0.0001);
		assertTrue("Player bet selection is wrong, should be ODD",
				placedBet.getBetType() == BetType.ODD);
	}

	/**
	 * Testing user missing bet amount User name Tiki_Monkey Position 2 Amount 1
	 */
	@Test
	public void testUserInputScenarioMissingBetAmount() throws Exception {

		roulette.loadPlayersFromFile("/test-players-no-history.txt");
		systemInMock.provideText("Tiki_Monkey ODD\n");
		roulette.readPlayerInput();

		Bet placedBet = roulette.getRouletteQueue().peek();
		assertNull(
				"No bet should be populated as the input parameters where incorrect",
				placedBet);

	}

}
