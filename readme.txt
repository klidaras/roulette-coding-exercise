A quick overview of my roulette project

The top level class with the static main is Roulette.
I spent all together about  8 hours on this project, is not finished for sure.

I should try to explain a bit how it all works and some points for development.

The intializeGame method calls initialize() which sets up a background thread with the ScheduledExecutorService running every 30 seconds.
If for any reason the thread dies, the service will schedule another one.

Concurreny is achieved with ConcurrentHashMap for the players and ArrayBlockingQueue for the bets.
ArrayBlockingQueue is the most efficient of all concurrent structures (a circular array with extremely very low contention).
To get better performance ou will need to go into disruptor patterns etc.

The method(turnTheWheel) drains the queue every 30 seconds and calculates payoffs.
The amounts are calculated on a bet level first and then aggregated for each player.

In order to write tests ( simple to start with and then more complex), the roulette had to work in a single spin mode.
Also lots of other aspects like scheduling of the timers had to be configurable. 
The logic that produces the random inputs has been modelled separately in order to replace it with a mock.
Look the TestRoulette class for more details.
I have tried to simulate depedency injection, allowing me to swap all the components with mocks. 

The keyboard input was mocked using this library
http://stefanbirkner.github.io/system-rules/

The test coverage of my tests is about 73%. 
Someone can spend weeks adding tests and functonality!!

Manually I tested the following boundary conditions
XXX                 --> Incorrect number of input parameters
XXX EVEN 3          --> Player name could be identified
Barbara             --> Incorrect number of input parameters
Barbara EVEN        --> Incorrect number of input parameters
Barbara EVEN XXX    --> Cannot convert into betting amount
Barbara XXXX 3.0    --> Cannot identify betting type
Barbara 44 3        --> The betting position must be between 1-36
Barbara 1 300000    --> Betting value must be less than 1000

What was not tested:
1. Performance. 

2. How it deals with resource loads ( big number of users, bets, low memory etc).
The system should have reactive properties regarding growth. 
If too many bets were to be placed, the blocking queue should be able to stop it.
(this is a better strategy than taking too much load and then failling to respond in time).

The above project was a proof of concept.
It was left running overnight to check  for resiliency and memory leaks.

On a second stage, I would consider splitting the rulette class into distinct components. (It does too much at the moment).
The class should only handle the business logic, the keyboard controller and the display functions should have be placed into different components.
Any communication between them should happen in a not blocking function.
Probably an event stage architecture could be a good fit ( SEDA ).

